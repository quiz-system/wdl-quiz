<?php 
session_start();
include 'connectdb.php';
if (!isset($_SESSION["name"]))
   {
      header("location: index.php");
   }
?>
<!DOCTYPE html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link rel="stylesheet" href="css/bootstrap.min.css" >
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/style-responsive.css" rel="stylesheet"/>
<!-- font CSS -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- font-awesome icons -->
<link rel="stylesheet" href="css/font.css" type="text/css"/>
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<script src="js/jquery2.0.3.min.js"></script>
</head>
<body>
<script type="text/javascript">
    $(document).ready(function(){
        $('#save').click(function() {

            let ques = $("#question").val();
            let opt1 = $("#option1").val();
            let opt2 = $("#option2").val();
            let opt3 = $("#option3").val();
            let opt4 = $("#option4").val();
            let ans = $("#answer").val();
            let email = "<?php echo $_SESSION['name']; ?>";
            $.ajax({
                type: "POST",
                 url: "validate.php",
                 dataType : 'text',
                data: ({ ques: ques , opt1:opt1, opt2:opt2, opt3:opt3, opt4:opt4, ans:ans, email:email }),
                success: function(data) {
                    if(data == "success"){
                        alert("added");
                        document.getElementById("questionform").reset();
                    }

                }

                    
            });
        });
    });

    

</script>







<section id="container">
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">

    <a href="Dashboard.php" class="logo">
        VIT QUIZ
    </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>
<!--logo end-->


<div class="top-nav clearfix">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img alt="" src="images/2.png">
                <span class="username"><?php echo $_SESSION['name'];?></span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                <li><a href="closedb.php"><i class="fa fa-key"></i> Log Out</a></li>
            </ul>
        </li>
        <!-- user login dropdown end -->
       
    </ul>
    <!--search & user info end-->
</div>
</header>
<!--header end-->
<!--sidebar start-->
<?php
    include 'sidebar.php';
?>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
            <section class="wrapper">
                <div class="form-w3layouts">
                    <!-- page start-->
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Add Questions
                                    <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-cog" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="form">
                                        <form class="cmxform form-horizontal " id="questionform" method="POST" action="#" onsubmit="return false" novalidate="novalidate">
                                            <div class="form-group ">
                                                <label for="firstname" class="control-label col-lg-3">Question</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="question" name="question" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="lastname" class="control-label col-lg-3">Option 1</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="option1" name="option1" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="username" class="control-label col-lg-3">Option 2</label>
                                                <div class="col-lg-6">
                                                    <input class="form-control " id="option2" name="option2" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="password" class="control-label col-lg-3">Option 3</label>
                                                <div class="col-lg-6">
                                                    <input class="form-control " id="option3" name="option3" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="username" class="control-label col-lg-3">Option 4</label>
                                                <div class="col-lg-6">
                                                    <input class="form-control " id="option4" name="option4" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="username" class="control-label col-lg-3">Answer</label>
                                                <div class="col-lg-6">
                                                    <input class="form-control " id="answer" name="answer" type="text">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 col-lg-6">
                                                    <button class="btn btn-primary" type="submit" name="save" id="save">Save</button>
                                                    
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                     <div class="clearfix"> </div>
                </div>
            </section>
            <!-- footer -->
            <div class="footer">
                <div class="wthree-copyright">
                    <p>© 2017 Visitors. All rights reserved</p>
                </div>
            </div>
            <!-- / footer -->
        </section>
        <!--main content end-->
    </section>





 <!-- footer -->

<script src="js/bootstrap.js"></script>
<script src="js/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scripts.js"></script>
<script src="js/jquery.slimscroll.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="js/jquery.scrollTo.js"></script>
</body>
</html>
