<?php 
session_start();
include 'connectdb.php';
if (!isset($_SESSION["name"]))
   {
      header("location: index.php");
   }

   error_reporting(1);
?>
<!DOCTYPE html>
<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- bootstrap-css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <!-- //bootstrap-css -->
    <!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <!-- font CSS -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- font-awesome icons -->
    <link rel="stylesheet" href="css/font.css" type="text/css"/>
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <script src="js/jquery2.0.3.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>

<section id="container">
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">

    <a href="Dashboard.php" class="logo">
        VIT QUIZ
    </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>
<!--logo end-->


<div class="top-nav clearfix">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img alt="" src="images/2.png">
                <span class="username"><?php echo $_SESSION['name'];?></span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                <li><a href="closedb.php"><i class="fa fa-key"></i> Log Out</a></li>
            </ul>
        </li>
        <!-- user login dropdown end -->
       
    </ul>
    <!--search & user info end-->
</div>
</header>
<!--header end-->
<!--sidebar start-->
<?php
    include 'sidebar.php';
?>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
        <section class="wrapper">
                <div class="form-w3layouts">
                    <!-- page start-->
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Attempt Quiz
                                    <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-cog" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="form">
                                        <form class="cmxform form-horizontal " id="questions" method="POST" action="attempt_quiz.php" >
                                    <?php
                               //$count = 1;
                               $email = $_SESSION['name'];
                               $qid = '';
                               $query = "SELECT qid, sname from start_quiz LEFT JOIN quiz_attempted USING(qid, sname) WHERE quiz_attempted.qid is null and quiz_attempted.sname is null";

                                $result = mysqli_query($conn,$query);
                                while($row = mysqli_fetch_array($result))
                                {
                                    $qid = $row['qid'];
                                    $sname = $row['sname'];
                                    ?>             
                                <div class="alert alert-success" role="alert" id="qid" >
                                        <?php echo $row['sname']; ?>
                                </div>
                                <?php } ?>
                                
                                <?php 
                                    $q = "select * from start_quiz where qid = '$qid'";
                                    $result = mysqli_query($conn,$q);
                                    while($row = mysqli_fetch_array($result))
                                    {
                                        $duration = $row['duration'];
                                        $noq = $row['noq'];
                                    }
                                ?>
                                
                                <div id="ques" style="display: none;">
                                <?php 
                               
                                    $i = 0;
                                   $q = "select * from questions limit $noq";
                                    $result = mysqli_query($conn,$q);
                                    while($row = mysqli_fetch_array($result))
                                    {
                                        $i = $row['id'];
                                    
                                ?>
                                <div class="card" >
                                        <div class="card-header">
                                               <?php  echo $row['ques']; ?>
                                             </div>
                             <div class="card-body">
                            <p class="card-text">    
                            <label ><input type="radio"  name="question[<?php echo $i; ?>]" value="<?php echo $row['opt1']; ?>"><?php echo " ".$row['opt1'];  ?></label><br/>
                            <label><input type="radio"  name="question[<?php echo $i; ?>]" value="<?php  echo $row['opt2']; ?>"><?php echo " ".$row['opt2'];  ?></label><br/>
                            <label ><input type="radio"  name="question[<?php echo $i; ?>]" value="<?php  echo $row['opt3']; ?>"><?php echo " ".$row['opt3'];  ?></lable><br/>
                            <label ><input type="radio"  name="question[<?php echo $i; ?>]" value="<?php echo $row['opt4']; ?>"><?php echo " ".$row['opt4'];  ?></label>
                            </p>
                            </div>
                                </div>
                        <?php }  ?>
                        </div>
                                <div  style="display: none;" id="submitbtn">
                                <center><button type="submit" value="submit" class="btn btn-success m-auto d-block" name="submit">submit</button></center>
                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                  <div class="clearfix"> </div>
                </div>
            </section>
            <!-- footer -->
            <br/><br/><br/><br/><br/><br/><br/><br/><br/>
            <div class="footer">
                <div class="wthree-copyright">
                    <p>© 2017 Visitors. All rights reserved</p>
                </div>
            </div>
            <!-- / footer -->
        </section>
        <!--main content end-->
    </section>

 <!-- footer -->

<script>

    $(document).ready(function(){
        $('#qid').click(function(){
            $('#ques').css("display", "block");
            $('#submitbtn').css("display", "block");
            
        });

    });

</script>
<?php 
    
    $i = 1;
    $marks = 0;
    $noq = '';
    $sname = '';
    if(isset($_POST['submit']))
    {
        if(!empty($_POST['question']))
        {
            $selected  = $_POST['question'];
           

            $q = "select id, ans from questions limit $noq";

            $query = mysqli_query($conn, $q);
            while($row = mysqli_fetch_array($query)){
                $i = $row['id'];
                $checked = $row['ans'] == $selected[$i];
                if($checked)
                {
                    $marks++;
                }
                $i++;
            }
            $q = "insert into result(id, email, marks) values(NULL, '$email', '$marks')";
            $q1 = "insert into quiz_attempted(id, qid, email, sname, marks) values(NULL, '$qid', '$email', '$sname', $marks)";
            $res = mysqli_query($conn, $q);
            $res1 = mysqli_query($conn, $q1);
            if($res == true && $res1 == true)
            {
                echo "<script>let marks = <?php echo $marks; ?></script>";
                echo "<script>swal('Congrats', 'Your Quiz is completed successfully!', 'success');</script>";
            } 
        }
    }

?>


<script src="js/bootstrap.js"></script>
<script src="js/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scripts.js"></script>
<script src="js/jquery.slimscroll.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="js/jquery.scrollTo.js"></script>
</body>
</html>
