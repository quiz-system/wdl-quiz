<?php
include 'connectdb.php';
if (!isset($_SESSION["name"]))
   {
      header("location: index.php");
   }
?>
<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class="active" href="Dashboard.php">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>

                    </a>
                </li>
                
                <!-- principal sidebar start -->
                <?php if($_SESSION['type'] === "principal"){ ?>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-tasks"></i>
                        <span>User Management</span>
                    </a>
                    <ul class="sub">
                        <li><a href="add_user.php">Add User</a></li>
                        <li><a href="update_user.php">Update User</a></li>
                        <li><a href="delete_user.php">Delete User</a></li>
                    </ul>
                </li>
                 <?php } ?>
                <!-- principal sidebar end -->

                <!---hod side bar start -->
                <?php if($_SESSION['type'] === "hod"){ ?>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-tasks"></i>
                        <span>User Management</span>
                    </a>
                    <ul class="sub">
                        <li><a href="add_user.php">Add User</a></li>
                        <li><a href="update_user.php">Update User</a></li>
                        <li><a href="delete_user.php">Delete User</a></li>
                    </ul>
                </li>
                
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa fa-bar-chart-o"></i>
                        <span>Performance</span>
                    </a>
                    <ul class="sub">
                        <li><a href="result.php">Class Wise</a></li>
                    </ul>
                </li> <?php } ?>

            <!-- hod sidebar end -->
            
            <!-- teacher sidebar start -->
           <?php if($_SESSION['type'] === "teacher"){ ?>
            <li class="sub-menu"><a href="javascript:;">
                        <i class="fa fa-th"></i>
                        <span>Quiz</span>
                    </a>
                    <ul class="sub">
                        <li><a href="start_quiz.php">Start Quiz</a></li>
                        <li><a href="delete_quiz.php">Delete Quiz</a></li>
                        <li><a href="add_question.php">Add Questions</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-tasks"></i>
                        <span>Student</span>
                    </a>
                    <ul class="sub">
                        <li><a href="add_user.php">Add Student</a></li>
                        <li><a href="update_user.php">Update Student</a></li>
                        <li><a href="delete_user.php">Delete Student</a></li>
                    </ul>
                </li>
                
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa fa-bar-chart-o"></i>
                        <span>Performance</span>
                    </a>
                    <ul class="sub">
                        <li><a href="result.php">Class Wise</a></li>
                    </ul>
                </li> <?php } ?>

                <!-- teacher sidebar end -->



            <!-- student sidebar strat -->
            <?php if($_SESSION['type'] === "student"){ ?>
            <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-th"></i>
                        <span>Quiz</span>
                    </a>
                    <ul class="sub">
                        <li><a href="attempt_quiz.php">Attempt Quiz</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa fa-bar-chart-o"></i>
                        <span>Performance</span>
                    </a>
                    <ul class="sub">
                        <li><a href="result.php">Subject wise</a></li>
                    </ul>
                </li> <?php } ?>

                <!--- student sidebar end -->
            </ul> 
        </div>
    </div>
</aside>