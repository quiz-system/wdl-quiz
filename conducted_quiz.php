<?php
session_start();
include 'connectdb.php';\
if (!isset($_SESSION["name"]))
   {
      header("location: index.php");
   }
?>
<!DOCTYPE html>
<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- bootstrap-css -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <!-- //bootstrap-css -->
    <!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <!-- font CSS -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- font-awesome icons -->
    <link rel="stylesheet" href="css/font.css" type="text/css"/>
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <script src="js/jquery2.0.3.min.js"></script>
</head>
<body>
    <section id="container">
        <!--header start-->
        <header class="header fixed-top clearfix">
            <!--logo start-->
            <div class="brand">
                <a href="Dashboard.php" class="logo">
                    VIT QUIZ
                </a>
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars"></div>
                </div>
            </div>
            <!--logo end-->
            <div class="top-nav clearfix">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" src="images/2.png">
                            <span class="username"><?php echo $_SESSION['name'];?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                            <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                            <li><a href="closedb.php"><i class="fa fa-key"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                    
                </ul>
                <!--search & user info end-->
            </div>
        </header>
        <!--header end-->
        <!--sidebar start-->
        <?php
        include 'sidebar.php';
        ?>
        <!--sidebar end-->
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <!-- //market-->
                <div class="market-updates">
                    <div class="col-md-4 market-update-gd" id="cmpn" onclick="table(this.id)">
                        <div class="market-update-block clr-block-2">
                            <div class="col-md-4 market-update-right" >
                                <i class="fas fa-desktop"></i>
                            </div>
                            <div class="col-md-8 market-update-left">
                                <h4>CMPN</h4>
                                
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="col-md-4 market-update-gd" id="it" onclick="table(this.id)">
                        <div class="market-update-block clr-block-1">
                            <div class="col-md-4 market-update-right">
                                <i class="fas fa-laptop"></i>
                            </div>
                            <div class="col-md-8 market-update-left">
                                
                                <h4>IT</h4>
                                
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="col-md-4 market-update-gd" id="extc" onclick="table(this.id)">
                        <div class="market-update-block clr-block-1">
                            <div class="col-md-4 market-update-right">
                                <i class="fa fa-microchip" ></i>
                            </div>
                            <div class="col-md-8 market-update-left">
                                <h4>EXTC</h4>
                                
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    
                    <br><br><br><br><br>
                    <div class="col-md-4 market-update-gd" id="etrx" onclick="table(this.id)">
                        <div class="market-update-block clr-block-1">
                            <div class="col-md-4 market-update-right">
                                <i class="fa fa-microchip" ></i>
                            </div>
                            <div class="col-md-8 market-update-left">
                                <h4>ETRX</h4>
                                
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="col-md-4 market-update-gd" id="biom" onclick="table(this.id)">
                        <div class="market-update-block clr-block-3">
                            <div class="col-md-4 market-update-right">
                                <i class="fa fa-microchip"></i>
                            </div>
                            <div class="col-md-8 market-update-left">
                                <h4>BIOM</h4>
                                
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="col-md-4 market-update-gd" id="mms" onclick="table(this.id)">
                        <div class="market-update-block clr-block-4">
                            <div class="col-md-4 market-update-right">
                                <i class="fa fa-microchip" aria-hidden="true"></i>
                            </div>
                            <div class="col-md-8 market-update-left">
                                <h4>MMS</h4>
                                
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>
                <!-- //market-->
                
            </section>
            
        </section>
        <section id="main-content">
            <section class="wrapper" >
                <div class="table-agile-info" style="display: none;" id="table" >
                    <div class="panel panel-default" >
                        <div class="panel-heading" id="tbl_heading">
                            Basic table
                        </div>
                        <div>
                            <table class="table" ui-jq="footable" ui-options='{
                                "paging": {
                                "enabled": true
                                },
                                "filtering": {
                                "enabled": true
                                },
                                "sorting": {
                                "enabled": true
                                }}'>
                                <thead>
                                    <tr>
                                        <th data-breakpoints="xs">ID</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th data-breakpoints="xs">Job Title</th>
                                        
                                        <th data-breakpoints="xs sm md" data-title="DOB">Date of Birth</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr data-expanded="true">
                                        <td>1</td>
                                        <td>Dennise</td>
                                        <td>Fuhrman</td>
                                        <td>High School History Teacher</td>
                                        
                                        <td>July 25th 1960</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Elodia</td>
                                        <td>Weisz</td>
                                        <td>Wallpaperer Helper</td>
                                        
                                        <td>March 30th 1982</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <!-- footer -->
            <br/><br/><br/><br/><br/><br/>
            <div class="footer">
                <div class="wthree-copyright">
                    <p>© 2019 Visitors. All rights reserved<a href="http://w3layouts.com"></a></p>
                </div>
            </div>
            <!-- / footer -->
        </section>
        <!--main content end-->
    </section>
    <script src="js/bootstrap.js"></script>
    <script src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.slimscroll.js"></script>
    <script src="js/jquery.nicescroll.js"></script>
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
    <script src="js/jquery.scrollTo.js"></script>
    <script type="text/javascript">
    function table(clicked_id){
    
    if(clicked_id == 'cmpn'){
    console.log(clicked_id);
    document.getElementById("tbl_heading").innerHTML = "Computer Department";
    }
    else if(clicked_id == 'it'){
    console.log(clicked_id);
    document.getElementById("tbl_heading").innerHTML = "Information Technology Department";
    }
    else if(clicked_id == 'extc'){
    console.log(clicked_id);
    document.getElementById("tbl_heading").innerHTML = "Electronics Department";
    }
    else if(clicked_id == 'etrx'){
    console.log(clicked_id);
    document.getElementById("tbl_heading").innerHTML = "Electronics & Telecommunication Department";
    }
    else if(clicked_id == 'biom'){
    console.log(clicked_id);
    document.getElementById("tbl_heading").innerHTML = "Biomedical Department";
    }
    document.getElementById("table").style.display = "block";
    }
    
    </script>
</body>
</html>