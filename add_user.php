<?php
session_start();
include 'connectdb.php';
if (!isset($_SESSION["name"]))
   {
      header("location: index.php");
   }
?>
<!DOCTYPE html>
<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- bootstrap-css -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <!-- //bootstrap-css -->
    <!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <!-- font CSS -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- font-awesome icons -->
    <link rel="stylesheet" href="css/font.css" type="text/css"/>
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <script src="js/jquery2.0.3.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>

<?php
    if(isset($_POST['save']))
    {
    $res = '';
    $iq2 = "";
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $type = $_POST['optradio'];
    
    $iq2 = "select * from users where email = '$username'";
    $res = mysqli_query($conn, $iq2);
    $row = mysqli_num_rows($res);
    if($row > 0){
        echo "<script>swal('Sorry', 'Username is already exist', 'error');</script>";
    }
    else{
         if($type === '1'){
    $iq = "insert into hod(id, fname, lname, user_name, password) values(NULL, '$fname', '$lname', '$username', '$password') ";
    $iq2 = "insert into users(id, email, type) values(NULL, '$username', 'hod')";
    }
    if($type === '2'){
    $iq = "insert into teacher(id, fname, lname, user_name, password) values(NULL, '$fname', '$lname', '$username', '$password')";
    $iq2 = "insert into users(id, email, type) values(NULL, '$username', 'teacher')";
    }
    if($type === '3'){
    $iq = "insert into student(id, fname, lname, user_name, password) values(NULL, '$fname', '$lname', '$username', '$password')";
    $iq2 = "insert into users(id, email, type) values(NULL, '$username', 'student')";
    }
    $res = mysqli_query($conn, $iq) or die(mysqli_error($conn));
    mysqli_query($conn, $iq2) or die(mysqli_error($conn));
    if($res == true){
    echo "<script>swal('Good Job', ' ', 'success');</script>";
        }
    }

   
    }
    
    ?>



    <section id="container">
        <!--header start-->
        <header class="header fixed-top clearfix">
            <!--logo start-->
            <div class="brand">
                <a href="Dashboard.php" class="logo">
                    VIT QUIZ
                </a>
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars"></div>
                </div>
            </div>
            <!--logo end-->
            <div class="top-nav clearfix">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">
                    
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" src="images/2.png">
                            <span class="username"><?php echo $_SESSION['name'];?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                            <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                            <li><a href="closedb.php"><i class="fa fa-key"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                    
                </ul>
                <!--search & user info end-->
            </div>
        </header>
        <!--header end-->
        <!--sidebar start-->
<?php
include 'sidebar.php';
?>
        <!--sidebar end-->
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <div class="form-w3layouts">
                    <!-- page start-->
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Add User
                                    <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-cog" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="form">
                                        <form class="cmxform form-horizontal " id="signupForm" method="POST" action="#" novalidate="novalidate">
                                            <div class="form-group ">
                                                <label for="firstname" class="control-label col-lg-3">Firstname</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="firstname" name="fname" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="lastname" class="control-label col-lg-3">Lastname</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="lastname" name="lname" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="username" class="control-label col-lg-3">Username</label>
                                                <div class="col-lg-6">
                                                    <input class="form-control " id="username" name="username" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="password" class="control-label col-lg-3">Password</label>
                                                <div class="col-lg-6">
                                                    <input class="form-control " id="password" name="password" type="password">
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="form-group ">
                                                <label for="email" class="control-label col-lg-3">User Type</label>
                                                <div class="col-lg-6"style="margin-top: 5px;">
                                                    <?php if($_SESSION['type'] === 'principal'){ ?>
                                                    <label><input type="radio" name="optradio" value="1">&nbsp;&nbsp;&nbsp;HOD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                                    <label><input type="radio" name="optradio" value="2">&nbsp;&nbsp;Teacher&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  </label>
                                                    <label><input type="radio" name="optradio" value="3">&nbsp;&nbsp;&nbsp;student</label> <?php } ?>
                                                    <?php if($_SESSION['type'] === 'hod'){ ?>
                                                    <label><input type="radio" name="optradio" value="2">&nbsp;&nbsp;Teacher&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  </label>
                                                    <label><input type="radio" name="optradio" value="3">&nbsp;&nbsp;&nbsp;student</label> <?php } ?>
                                                    <?php if($_SESSION['type'] === 'teacher'){ ?>
                                                    <label><input type="radio" name="optradio" value="3" checked>&nbsp;&nbsp;&nbsp;student</label> <?php } ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 col-lg-6">
                                                    <button class="btn btn-primary" type="submit" name="save">Save</button>
                                                    <button class="btn btn-default" type="button">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <!-- page end-->
                </div>
            </section>
            <!-- footer -->
            <div class="footer">
                <div class="wthree-copyright">
                    <p>© 2017 Visitors. All rights reserved</p>
                </div>
            </div>
            <!-- / footer -->
        </section>
        <!--main content end-->
    </section>
    <script src="js/bootstrap.js"></script>
    <script src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.slimscroll.js"></script>
    <script src="js/jquery.nicescroll.js"></script>
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
    <script src="js/jquery.scrollTo.js"></script>
</body>
</html>