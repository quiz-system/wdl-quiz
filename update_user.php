<?php
session_start();
include 'connectdb.php';
if (!isset($_SESSION["name"]))
   {
      header("location: index.php");
   }
?>
<!DOCTYPE html>
<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- bootstrap-css -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <!-- //bootstrap-css -->
    <!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <!-- font CSS -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- font-awesome icons -->
    <link rel="stylesheet" href="css/font.css" type="text/css"/>
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <script src="js/jquery2.0.3.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>
<?php

    $fname ='';
    $lname = '';
    $uname = '';
    $pass = '';
    $q = '';
    $row = "";
    $del = "";

    if(isset($_POST['update'])){
    $username = $_POST['update'];

    $sq = "select type from users where email='$username'";
    $result = mysqli_query($conn, $sq) or die(mysqli_error($conn));
    $row  = mysqli_fetch_assoc($result);
    if($row != NULL){
            $del = $row['type'];


    if($_SESSION['type'] == "principal"){
      if($row['type'] == "principal"){
        $q = "select * from principal where user_name = '$username'";
        $result = mysqli_query($conn, $q) or die(mysqli_error($conn));
    while ($row = mysqli_fetch_array($result)){     
    
    $fname = $row['fname'];
    $lname = $row['lname'];
    $uname = $row['user_name'];
    $pass = $row['password'];
    }
    }
    else if($row['type'] == "hod"){
        $q = "select * from hod where user_name = '$username'";
        $result = mysqli_query($conn, $q) or die(mysqli_error($conn));
    while ($row = mysqli_fetch_array($result)){     
  
    $fname = $row['fname'];
    $lname = $row['lname'];
    $uname = $row['user_name'];
    $pass = $row['password'];
    }
    }
    else if($row['type'] == "teacher"){
        $q = "select * from teacher where user_name = '$username'";
        $result = mysqli_query($conn, $q) or die(mysqli_error($conn));
    while ($row = mysqli_fetch_array($result)){     
 
    $fname = $row['fname'];
    $lname = $row['lname'];
    $uname = $row['user_name'];
    $pass = $row['password'];
    }
    }
    else if($row['type'] == "student"){
        $q = "select * from student where user_name = '$username'";
        $result = mysqli_query($conn, $q) or die(mysqli_error($conn));
    while ($row = mysqli_fetch_array($result)){     
   
    $fname = $row['fname'];
    $lname = $row['lname'];
    $uname = $row['user_name'];
    $pass = $row['password'];
    }
    }
      
    }

    if($_SESSION['type'] == "hod"){
    if($row['type'] == "hod"){
        $q = "select * from hod where user_name = '$username'";
        $result = mysqli_query($conn, $q) or die(mysqli_error($conn));
    while ($row = mysqli_fetch_array($result)){     

    $fname = $row['fname'];
    $lname = $row['lname'];
    $uname = $row['user_name'];
    $pass = $row['password'];
    }
    }
    else if($row['type'] == "teacher"){
        $q = "select * from teacher where user_name = '$username'";
        $result = mysqli_query($conn, $q) or die(mysqli_error($conn));
    while ($row = mysqli_fetch_array($result)){     
    $fname = $row['fname'];
    $lname = $row['lname'];
    $uname = $row['user_name'];
    $pass = $row['password'];
    }
    }
    else if($row['type'] == "student"){
        $q = "select * from student where user_name = '$username'";
        $result = mysqli_query($conn, $q) or die(mysqli_error($conn));
    while ($row = mysqli_fetch_array($result)){     
    $fname = $row['fname'];
    $lname = $row['lname'];
    $uname = $row['user_name'];
    $pass = $row['password'];
    }
    }
   
      
    }

    if($_SESSION['type'] == "teacher"){
    if($row['type'] == "student"){
      
        $q = "select * from student where user_name = '$username'";
        $result = mysqli_query($conn, $q) or die(mysqli_error($conn));
    while ($row = mysqli_fetch_array($result)){     
    $fname = $row['fname'];
    $lname = $row['lname'];
    $uname = $row['user_name'];
    $pass = $row['password'];
    }
    } 
     
    }

    }
    if($row == NULL){
        echo "<script>swal('Sorry', 'Record Not Found!', 'error');</script>";
    }
  
  }  
?>

<script type="text/javascript">
$(document).ready(function(){
    $("#updater").click(function(){
        let fname = $("#firstname").val();
        let lname = $("#lastname").val();
        let uname = $("#username").val();
        let pass = $("#password").val();
        let utype = "<?php echo $del ?>";
        $.ajax({

            url:"validate.php",
            method:"POST",
            data:({fname:fname, lname:lname, uname:uname, pass:pass, utype:utype}),
            success:function(data){
            if(data == "success")
            {
               swal("Good job!", "You updated one record!", "success");   
            }
        }
        });

    });
});   

</script>

<section id="container">
    <!--header start-->
    <header class="header fixed-top clearfix">
        <!--logo start-->
        <div class="brand">
            <a href="Dashboard.php" class="logo">
                VIT QUIZ
            </a>
            <div class="sidebar-toggle-box">
                <div class="fa fa-bars"></div>
            </div>
        </div>
        <!--logo end-->
        <div class="top-nav clearfix">
            <!--search & user info start-->
            <ul class="nav pull-right top-menu">
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <img alt="" src="images/2.png">
                        <span class="username"><?php echo $_SESSION['name'];?></span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                        <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                        <li><a href="closedb.php"><i class="fa fa-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- user login dropdown end -->
                
            </ul>
            <!--search & user info end-->
        </div>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    include 'sidebar.php';
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="form-w3layouts">
                <!-- page start-->
                
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                                Update User Details
                                
                            </header>
                            <div class="panel-body">
                                <div class="form">
                                    <form class="cmxform form-horizontal " id="signupForm" method="POST" action="#" novalidate="novalidate" >
                                        <div id="upshow">
                                        <span class="tools pull-right">
                                            <input type="text" class="form-control search" placeholder=" Search" name="update">
                                        </span>
                                    </div>
                                    <?php if($fname != ''){ ?>
                        
                                        <div class="form-group ">
                                            <label for="firstname" class="control-label col-lg-3">Firstname</label>
                                            <div class="col-lg-6">
                                                <input class=" form-control" id="firstname" name="fname" type="text" value="<?php echo $fname; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="lastname" class="control-label col-lg-3">Lastname</label>
                                            <div class="col-lg-6">
                                                <input class=" form-control" id="lastname" name="lname" type="text" value="<?php echo $lname; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="username" class="control-label col-lg-3">Username</label>
                                            <div class="col-lg-6">
                                                <input class="form-control " id="username" name="username" type="text" value="<?php echo $uname; ?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="password" class="control-label col-lg-3">Password</label>
                                            <div class="col-lg-6">
                                                <input class="form-control " id="password" name="password" type="password" value="<?php echo $pass; ?>">
                                            </div>
                                        </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-3 col-lg-6">
                                                <button class="btn btn-primary" type="submit" name="update" id="updater">update</button>
                                                <button class="btn btn-default" type="button">Cancel</button>
                                            </div>
                                        </div>
                                   
                                <?php }?>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- page end-->
            </div>
        </section>
        <!-- footer -->
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <div class="footer">
            <div class="wthree-copyright">
                <p>© 2017 Visitors. All rights reserved</p>
            </div>
        </div>
<!-- / footer -->
</section>
        <!--main content end-->
    </section>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.slimscroll.js"></script>
    <script src="js/jquery.nicescroll.js"></script>
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
    <script src="js/jquery.scrollTo.js"></script>
</body>
</html>