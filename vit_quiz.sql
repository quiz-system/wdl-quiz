-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2019 at 08:08 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vit_quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `hod`
--

CREATE TABLE `hod` (
  `id` int(11) NOT NULL,
  `fname` varchar(25) NOT NULL,
  `lname` varchar(25) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hod`
--

INSERT INTO `hod` (`id`, `fname`, `lname`, `user_name`, `password`) VALUES
(1, 'hod', 'hod', 'hod@gmail.com', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `principal`
--

CREATE TABLE `principal` (
  `id` int(11) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `principal`
--

INSERT INTO `principal` (`id`, `user_name`, `password`) VALUES
(1, 'manish@gmail.com', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `ques` varchar(500) NOT NULL,
  `opt1` varchar(40) NOT NULL,
  `opt2` varchar(40) NOT NULL,
  `opt3` varchar(40) NOT NULL,
  `opt4` varchar(40) NOT NULL,
  `ans` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `ques`, `opt1`, `opt2`, `opt3`, `opt4`, `ans`, `email`) VALUES
(1, ' PHP files have a default file extension of_______', ' .html', '.xml', '.php', '.ph', '.php', 'admin@gmail.com'),
(2, 'What should be the correct syntax to write a PHP code?', ' < php >', '< ? php ?>', ' <? ?>', '<?php ?>', ' <? ?>', 'admin@gmail.com'),
(5, ' How to define a function in PHP?', 'function {function body}', 'data type functionName(parameters) {func', ' functionName(parameters) {function body', 'function functionName(parameters) {funct', ' function functionName(parameters) {func', 'admin@gmail.com'),
(6, 'A function in PHP which starts with __ (double underscore) is known as __________', ' Magic Function', 'Inbuilt Function', 'Default Function', 'User Defined Function', ' Magic Function', 'admin@gmail.com'),
(7, ' Which of the following PHP functions accepts any number of parameters?', ' func_get_argv()', 'func_get_args()', 'get_argv()', 'get_argc()', 'func_get_args()', 'admin@gmail.com'),
(8, ' Which one of the following PHP functions can be used to find files?', 'glob()', ' file()', ' fold()', 'get_file()', 'glob()', 'admin@gmail.com'),
(9, 'Which of the following PHP functions can be used to get the current memory usage?', 'get_usage()', 'get_peak_usage()', ' memory_get_usage()', ' memory_get_peak_usage()', ' memory_get_usage()', 'admin@gmail.com'),
(10, 'Which of the following PHP functions can be used for generating unique ids?', ' uniqueid()', 'id()', 'md5()', 'mdid()', ' uniqueid()', 'admin@gmail.com'),
(11, 'Which one of the following functions can be used to compress a string?', 'zip_compress()', ' zip()', ' compress()', 'gzcompress()', 'gzcompress()', 'admin@gmail.com'),
(12, '. PHPâ€™s numerically indexed array begin with position ___________', '1', '2', '0', '-1', '0', 'admin@gmail.com'),
(13, 'Which in-built function will add a value to the end of an array?', 'array_unshift()', ' into_array()', ' inend_array()', 'array_push()', ' array_push()', 'admin@gmail.com'),
(14, ' Which of the following function is used to get the value of the previous element in an array?', ' last()', 'before()', ' prev()', ' previous()', ' prev()', 'admin@gmail.com'),
(15, 'Which function returns an array consisting of associative key/value pairs?', 'count()', 'array_count()', 'array_count_values()', ' count_values()', 'array_count_values()', 'admin@gmail.com'),
(16, 'Which of the functions is used to sort an array in descending order?', ' sort()', ' asort()', 'rsort()', 'dsort()', 'rsort()', 'admin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_attempted`
--

CREATE TABLE `quiz_attempted` (
  `id` int(11) NOT NULL,
  `qid` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `sname` varchar(50) NOT NULL,
  `marks` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz_attempted`
--

INSERT INTO `quiz_attempted` (`id`, `qid`, `email`, `sname`, `marks`) VALUES
(3, 'php1', 'st@gmail.com', 'php', 4);

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `marks` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `email`, `marks`) VALUES
(1, 'st@gmail.com', 4);

-- --------------------------------------------------------

--
-- Table structure for table `start_quiz`
--

CREATE TABLE `start_quiz` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `qid` varchar(50) NOT NULL,
  `sname` varchar(50) NOT NULL,
  `noq` int(11) NOT NULL,
  `duration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `start_quiz`
--

INSERT INTO `start_quiz` (`id`, `email`, `qid`, `sname`, `noq`, `duration`) VALUES
(5, 'admin@gmail.com', 'php1', 'php', 10, 10);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `fname` varchar(25) NOT NULL,
  `lname` varchar(25) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `fname`, `lname`, `user_name`, `password`) VALUES
(1, 'st', 'st', 'st@gmail.com', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL,
  `fname` varchar(25) NOT NULL,
  `lname` varchar(25) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `fname`, `lname`, `user_name`, `password`) VALUES
(1, 'MANISH', 'YADAV', 'admin@gmail.com', '1234'),
(2, 'MANISH', 'YADAV', 'manishyadav7208@gmail.com', '1234'),
(3, 'MANISH', 'YADAV', 'manishyadav7208@gmail.com', '1234'),
(4, 'MANISH', 'YADAV', 'qwe@gmail.com', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `type` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `type`) VALUES
(1, 'admin@gmail.com', 'teacher'),
(2, 'hod@gmail.com', 'hod'),
(3, 'manish@gmail.com', 'principal'),
(6, 'manishyadav7208@gmail.com', 'teacher'),
(7, 'manishyadav7208@gmail.com', 'teacher'),
(8, 'qwe@gmail.com', 'teacher'),
(9, 'st@gmail.com', 'student');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hod`
--
ALTER TABLE `hod`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `principal`
--
ALTER TABLE `principal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_attempted`
--
ALTER TABLE `quiz_attempted`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `start_quiz`
--
ALTER TABLE `start_quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hod`
--
ALTER TABLE `hod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `principal`
--
ALTER TABLE `principal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `quiz_attempted`
--
ALTER TABLE `quiz_attempted`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `start_quiz`
--
ALTER TABLE `start_quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
