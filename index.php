<?php
session_start();
include 'connectdb.php';

?>
<!DOCTYPE html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script
	type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<link href="css/style-responsive.css" rel="stylesheet" />
	<!-- font CSS -->
	<link
		href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic'
		rel='stylesheet' type='text/css'>
		<!-- font-awesome icons -->
		<link rel="stylesheet" href="css/font.css" type="text/css" />
		<link href="css/font-awesome.css" rel="stylesheet">
		<!-- //font-awesome icons -->
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		
		<script>
			$(document).ready(function(){
				$("#login").click(function(){
					let email  = $("#email").val();
					let password  = $("#password").val();
					let login = "login";

					console.log(password.length);
					if(password.length <= 3){
						$('#notice').html("Password length must be greater than 3");
						return false;
					}
					else{
						$.ajax({
						url:"validate.php",
						method:"GET",
						data:{email:email,password:password,login:login},
						success:function(data){
							if(data=="success")
							{
									window.location.href = "Dashboard.php";
							}
							else{
									$('#notice').html("Invalid Credential");
							}
							
							
						}
					});
					}
				});
					
			});

		</script>
	</head>
	<body>
		<div class="log-w3">
			<div class="w3layouts-main">
				<h2>Sign In Now</h2>
				<form action="#" onsubmit="return false" method="POST" id="loginform">
					<input type="email" class="ggg" name="email" placeholder="E-MAIL" required="" autocomplete=off id="email">
					<input type="password" class="ggg" name="password" placeholder="PASSWORD" required="" id="password">
					<div class="clearfix"></div>
					<div id="notice" style="text-align: center; color: white; font-weight: bold;">
						</div>
					<input type="submit" value="Sign In" name="login" id="login">
				</form>
			</div>
		</div>
		
	</body>
</html>